db.fruits.aggregate([
    {$match:{supplier:"Red Farms Inc"}},
    {$count:"countSupplierOfRedFarms"}
]);

db.fruits.aggregate([
    {$match:{price:{$gt:50}}},
    {$count:"countGreaterThan50"}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier",averagePrice:{$avg:"$price"}}}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier",highest:{$max:"$price"}}}
])

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier",highest:{$min:"$price"}}}
])