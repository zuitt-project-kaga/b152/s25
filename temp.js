db.fruits.insertMany([
  {
    name: "Apple",
    supplier: "Red Farms Inc",
    stocks: 20,
    price: 40,
    onSale: true,
  },
  {
    name: "Banana",
    supplier: "Yellow Farms",
    stocks: 15,
    price: 20,
    onSale: true,
  },
  {
    name: "Kiwi",
    supplier: "Green Farming and Canning",
    stocks: 25,
    price: 50,
    onSale: true,
  },
  {
    name: "Mango",
    supplier: "Yellow Farms",
    stocks: 10,
    price: 60,
    onSale: true,
  },
]);

db.fruits.insertOne({
  name: "Dragon Fruit",
  supplier: "Red Farms Inc",
  stocks: 10,
  price: 60,
  onSale: true,
});

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:null,total:{$sum:"$stocks"}}}
]);

db.fruits.updateOne({},{$set:{onSale:false}});

// db.fruits.aggregate([
//     {$match:{onSale:true}},
//     {$group:{total:{$sum:"$stocks"}}}
// ]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"totalStock",total:{$sum:"$stocks"}}}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier",total:{$sum:"$stocks"}}}
]);

db.fruits.aggregate([
    {$match:{supplier:"Red Farms Inc"}},
    {$group:{_id:"redFarmsIncStocks",totalStock:{$sum:"$stocks"}}}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$suplier",avgStock:{$avg:"$stocks"}}}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"avgPriceOnSale",avgPrice:{$avg:"$price"}}}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:'maxStockOnSake',maxStock:{$max:"$stocks"}}}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:'$supplier',maxStock:{$max:"$stocks"}}}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"minStockOnSale",minStock:{$min:"$stocks"}}}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier",minStock:{$min:"$stocks"}}}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$count:"itemsOnSale"}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$min:"itemsOnSale"}
]);

db.fruits.aggregate([
    {$match:{price:{$lt:50}}},
    {$count:"priceLessThan50"}
]);

db.fruits.aggregate([
    {$match:{onSale:false}},
    {$count:"countNotOnSale"}
])

db.fruits.aggregate([
    {$match:{stocks:{$lt:20}}},
    {$count:"lessThan20Stock"}
])

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"countBySupplier",total:{$sum:1}}}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier",totalCount:{$sum:1} ,totalStock:{$sum:"$stocks"},itemPrices:{$addToSet:"$price"}}}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier",totalStock:{$sum:"$stocks"}}},
    {$out:"totalStocksPerSupplier"}
]);

db.fruits.aggregate([
    {$match:{supplier:"Red Farms Inc"}},
    {$count:"countSupplierOfRedFarms"}
]);

db.fruits.aggregate([
    {$match:{price:{$gt:50}}},
    {$count:"countGreaterThan50"}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier",averagePrice:{$avg:"$price"}}}
]);

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier",highest:{$max:"$price"}}}
])

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier",highest:{$min:"$price"}}}
])